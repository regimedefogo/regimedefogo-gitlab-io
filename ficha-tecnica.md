---
title: Ficha Técnica
layout: default
---

<div class="container-fluid">
  <div class="row">
<div class="ficha-tecnica col-sm-10">
  <h1>Ficha Técnica</h1>
  <p><b>Organização e curadoria:</b> Artur Cabral e Luciany Osório</p>
  
  <p><b>Assistentes de produção técnica e executiva:</b> Equipe MediaLab/UNB</p>

  <p><b><a href="mailto:medialabunbdf@gmail.com">Fale com a gente ;)</a></b></p>
  <p>
    <b>Artistas:</b><br>
    <span>
      Ana Hoeper <br>
      André Inácio <br>
      Clarice Gonçalves <br>
      Clarice Martins <br>
      Deiço Xavier <br>
      Ana Lúcia Canetti <br>
      Glenio Lima <br>
      Ianni Luna <br>
      José Loures <br>
      Jurema <br>
      Lynn Carone<br>
      Marlene Ribeiro <br>
      Marta Mencarini <br>
      Nycacia <br>
      Paula Castro <br>
      Priscila Portugal <br>
      Ricardo Stuckert <br>
      Robson Castro <br>
      Tainá Luize <br>
      Tatiana Duarte <br>
      Thiago Rodeghiero <br>
      Triz <br>
      Van Desart<br>
      Viníciu Fagundes <br>
    </span>
  </p>
</div>
</div>
</div>


