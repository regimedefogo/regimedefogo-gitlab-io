---
title: Sobre
layout: default
---

<div class="container-fluid">
  <div class="row">
    <div class="sobre col-sm-10">
      <h5>
      NO fogo há milênios desperta o fascínio e o pavor em nós, seres humanos. Responsável pela transformação de materiais e de paisagens, pelo calor que abraça e faz perecer o frio, o fogo também flerta com a destruição e a morte. Alimentado pelas correntes de ar seco que percorrem as vastas terras do Cerrado brasileiro, o fogo deixa o chão como braseiro que se retroalimenta enquanto a água não chega para abrandar sua fúria. <br>
  		Com a crescente ampliação de áreas para o cultivo de monoculturas e criação de gado, o bioma do Cerrado passou a ser a maior vítima de incêndios e desmatamentos indiscriminados no Brasil, mas o fogo não representa somente a destruição. Um regime de fogo ecologicamente apropriado garante o rebrotamento e a germinação de muitas espécies nativas do Cerrado, o que nos faz lembrar que a diferença entre o remédio e o veneno é a dose prescrita. <br>
  		Com o agravamento dos processos de degradação da natureza, inquietações pairam acerca da sobrevivência e modos de vida em um futuro breve, para várias espécies, inclusive a humana.  Em nome do progresso, os usos e abusos da natureza promovidos ao longo de séculos agora cobram seu preço. A compressão dos processos da natureza, que possibilitam a coexistência do planeta e dos seres que nele habitam, é parte dos ensinamentos culturais dos povos originários e que a muito são ignorados pelos incautos que se denominam civilizados. <br>
  		A crise ambiental que hoje vivemos pede uma nova postura dos artistas, instigando posicionamentos que reafirmem a função social da arte alinhada às demandas urgentes desses tempos de incertezas. Diante dessas questões urgentes, que envolvem o meio ambiente, artistas têm buscado se apropriar e intervir no debate, através de reflexões e provocações poéticas que nos conduzem a percepção da nossa finitude ao nos relembrar da nossa dependência vital da natureza.<br>
  		Em um movimento de enfrentamento dessas questões a mostra Regime de Fogo flerta com as aproximações poéticas da arte contemporânea e as implicações do ser e do viver neste espaço que compreende o Cerrado. Os regimes de fogo que expõem a resiliência deste bioma, que transita no limite do viver e do morrer, servem como pano de fundo para a criação dessa web-exposição que contempla obras que nos aproxima das reflexões sobre a necessidade de preservação desse bioma ao mesmo tempo em que se enlaçam na própria natureza dos processos de criação artística, tantas vezes disruptivos e regeneradores. Assim como o fogo faz eclodir sementes adormecidas a arte também se propõe a botar em movimento e transmutar sentimentos e pensamentos enraizados através de provocações estéticas.<br>
  		A mostra Regime de Fogo conta com a participação de 24 artistas que coabitam o bioma do Cerrado e que posicionam seu fazer artístico em um diálogo a partir desse lócus, por meio do corpo, de intervenções e ações em espaços públicos ou privados, naturais e urbanos, criando obras com materiais não convencionais colhidos em atenção ao ambiente, retratando a ligação dos seres com a terra em seus períodos de dormência e germinação, mantendo o olhar voltado para uma crítica sobre o lugar e a ação do homem junto ao meio ambiente. <br>
  		A Web-exposição Regime de Fogo foi concebida para fazer parte da programação cultural da II Semana Integrada do Cerrado, cujo tema é "Cerrado: saberes, usos e abusos". O evento é uma realização do Instituto Federal de Goiás e conta com o apoio de 27 instituições de ensino públicas, privadas e organizações da sociedade civil, envolvendo os estados de Goiás, Tocantins, Maranhão e o Distrito Federal.<br>
      </h5>
      <p class='nome-autor'>Luciany Osório, 2022</p>
    </div>
  </div>
</div>
