---
layout: obra
title: Fogo Aberto
thumbnail: /assets/thumbnail/t-fogo.png
artista: Marilene Ribeiro
l-bio: https://www.marileneribeiro.com/
t-bio: Marilene Ribeiro é artista visual, ecóloga e pesquisadora. Sua prática hibridiza disciplinas e mescla mídias e assuntos relacionados aos Direitos Humanos e da Natureza. É doutora em Artes Criativas/Fotografia pela University for the Creative Arts (Inglaterra), mestre em Ecologia, Conservação e Manejo de Vida Silvestre pela Universidade Federal de Minas Gerais (UFMG). Frequentou a Escola de Belas Artes da UFMG, a Central Saint Martins da University of the Arts London (Reino Unido) e recebeu treinamento pela Magnum Photos. Atualmente, é pesquisadora associada da Escola de Artes, Línguas e Culturas da Universidade de Manchester (Reino Unido) e membro associada do Centre for Spatial, Environmental and Cultural Politics da Universidade de Brighton (Reino Unido). Tem trabalhos premiados pelo PhotoEspaña, Royal Photographic Society, Funarte, Museu da Imagem e do Som - MIS/SP, Prêmio Esso de Jornalismo, finalistas a prêmios do Les Encontres d’Arles de La Photographie, Encontros da Imagem Photography and Visual Arts Festival, Biennial for Fine Art & Documentary Photography e Marilyn Stafford FotoReportage Award. Tem trabalhos exibidos na América Latina, Europa, Reino Unido, EUA e China, ensaios sobre suas obras publicados em plataformas especializadas em fotografia/artes visuais, como LensCulture, Photoworks, Contemporary Photography Journal, Viens Voir, PhMuseum, Foto Féminas, Katalog, Resumo Fotográfico e The Royal Photographic Society Journal. 
texto-descricao: Com foco nos incêndios que acontecem de maneira marcante na atualidade dentro do território brasileiro, FOGO ABERTO (ou ABRIR FOGO) incorpora a violência desses atos contra o patrimônio natural e cultural e expõe os danos provocados pelos mesmos, em uma narrativa plurivocal. FOGO ABERTO (ou ABRIR FOGO) é o resultado das minhas reflexões como artista visual, pesquisadora e pensadora da imagem e como cidadã, frente à situação que estamos vivenciando de perda acelerada e violenta do nosso patrimônio natural e cultural através da ação do fogo. Estarrecidos, temos vivenciado, nestes últimos anos, momentos de angústia e indignação frente ao fogo que impiedosamente devora o Brasil, consumindo paisagens, vidas e a nossa própria identidade. As imagens e os números divulgados são tão alarmantes que têm gerado um estado de choque coletivo frente à gravidade da situação e à dimensão das perdas. Afinal, o que resta após a passagem desse fogo, que violenta a terra e a vida sobre ela, colocando em risco nosso patrimônio? O que sobrará para o futuro, para ser visto, reconhecido e relembrado? Esse fogo me faz refletir sobre a vulnerabilidade e a fragilidade das coisas&#58;	 das coisas que são parte da nossa história e da nossa identidade (como indivíduos, como comunidade e como sociedade). E da importância em preservá-las. A obra foi concebida da seguinte maneira&#58;	 primeiramente, realizei fotografias de áreas naturais de alta relevância para a conservação no Brasil com câmera analógica de médio formato. Em seguida, tais películas fotográficas reveladas foram sistematicamente queimadas por mim – reproduzindo-se, assim, no suporte que carrega a representação desse locais de grande valor imaterial, a agressão que lhes tem sido infligida através da ação do fogo. Após a queima, o material resultante foi digitalizado, editado e colocado em diálogo com fatos reais publicados nas diversas mídias sobre o assunto incêndios/queimadas e também com reflexões minhas durante a produção da obra, ou seja, reflexões e situações que vivencio no meu cotidiano em relação ao tema. Assim, nesta obra, cada um desses fatos ou pensamentos é apresentado sob a forma de texto e audio (narração) em associação com uma imagem, embebendo cada imagens da obra do contexto maior onde a mesma está inserida. Essa interação imagem-som-texto da obra FOGO ABERTO pode ser conferida no website que a hospeda (www.openfireart.com) .Apesar de ter como base imagens da natureza, este trabalho não se limita a remeter somente à destruição do patrimônio natural, mas pretende evocar (por meio dessas paisagens naturais que sofrem a minha intervenção) o apagamento através da ação do fogo que acontece de forma mais ampla no Brasil e que envolve também o patrimônio histórico e cultural – tomemos como exemplo o incêndio do Museu Nacional.Assim, FOGO ABERTO (ou ABRIR FOGO) acontece como um convite ao debate sobre questões socioambientais e históricas prementes, já que esta obra aborda tema de extrema relevância e urgência para a sociedade brasileira ao lembrar que, mais uma vez, estamos na estação das queimadas, não somente nas áreas de Cerrado, como também na Amazônia e região Pantaneira.
ano: 2022
tecnica: Multimídia (fotografia, intervenção com fogo, som e texto)
---
<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">


   <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>

   </ul>
    
   <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/marilene/fogo1.png" alt="Fogo" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/marilene/fogo2.png" alt="Fogo" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/marilene/fogo3.png" alt="Fogo" class="img-fluid mx-auto d-block">
      </div>

    </div>
    
   <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
<br>
<a href="http://www.openfireart.com/pt/"><code class="highlighter-rouge"> Website Openfire </code></a>






<div class="d-block d-sm-none">
    <img src="/assets/obras/marilene/fogo1.png" alt="Fogo" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/marilene/fogo2.png" alt="Fogo" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/marilene/fogo3.png" alt="Fogo" class="img-fluid" width="35%">
</div>



