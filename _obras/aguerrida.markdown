---
layout: obra
title: Aguerrida
thumbnail: /assets/thumbnail/t-aguerrida.png
artista: Van Desart
l-bio: https://www.instagram.com/van.desart/
t-bio: Van Desart é goiana de gerações, paisagista, designer e artista visual, com raízes tão profundas quanto as do cerrado, busca em seu trabalho instigar reflexões e questionamentos políticos, socioculturais e ambientais. Em setembro de 2021, foi selecionada para o programa do governo de Goiás, o “Bolsa artista”, onde atualmente o grupo desenvolve um projeto de valoração do bioma cerrado, com produções artísticas, palestras, oficinas, exposições entre outros. Faz parte do grupo Plantadores de Água e meus projetos paisagísticos sempre são voltados para valoração e conhecimento de nossa flora. Usa todo o seu conhecimento botânico adquirido para criar artes com conteúdo científico e como meio de informar sobre as questões ambientais. 
texto-descricao: De acordo com o dicionário, diz-se aguerrida aquela que é belicosa, valente, destemida e que não esmorece perante as contrariedades, e assim são também muitas  árvores do cerrado. Nesta tela, apresento uma árvore de cor vermelha para mostrar a força e imponência de nosso bioma. Ao fundo, a serra dourada, iluminada pela luz do luar. O cerrado não teme o fogo, ele se prepara para recebê-lo, como em uma dança de vida e morte, suas cascas grossas funcionam como um mecanismo de defesa às queimadas; o fogo também ajuda na dispersão de sementes uma vez que algumas plantas precisam estar sob altas temperaturas para brotar. Apesar disso, os incêndios causados por ação humana e o desmatamento acelerado, tem sido devastador. Nosso bioma segue resistindo e ressurgido das cinzas, mas até quando?
ano: 2022
tecnica: pintura
---
<div class="menu d-none d-sm-block">


  <div id="imagens" class="carousel slide" data-ride="carousel">


   <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
     


   </ul>
    
   <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/van/aguerrida1.jpg" alt="Aguerrida" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/van/aguerrida2.jpg" alt="Aguerrida" class="img-fluid mx-auto d-block">
      </div>
      
  </div>
 
 
   <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
<br>
<code class="highlighter-rouge">70x100 cm</code>





<div class="d-block d-sm-none">
  
<img src="/assets/obras/van/aguerrida1.jpg" alt="Aguerrida" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/van/aguerrida2.jpg" alt="Aguerrida" class="img-fluid" width="35%">
  
</div>


