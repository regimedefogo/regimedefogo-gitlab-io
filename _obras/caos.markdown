---
layout: obra
title: Caos - Sol - Equilíbrio
thumbnail: /assets/thumbnail/t-caos.png
artista: Triz
l-bio: https://www.instagram.com/jardimdebeatriz/
t-bio: Bea(triz) de Oliveira Paiva é a artista plástica à frente do projeto @jardimdebeatriz e a escritora de cartas de amor do projeto @assinadovoce. Criativa, curiosa e comunicadora, ela transita entre os prazeres estéticos da arte e a ética do jornalismo. Afinal, são 20 anos de carreira, dos quais foram mais de 10 anos como editora de moda, cobrindo desfiles e produzindo ensaios fotográficos mundo afora. Múltipla, sempre flertou com as artes, adora fotografar e é aprendiz de cerâmica. Beatriz acumula experiências como empreendedora na área de eventos e também já foi servidora pública. Agora, dedica-se ao apronfundamento de sua poética a partir da plasticidade das pétalas.
texto-descricao: O trabalho traz à luz a relação de iconofagia entre a pessoa e a paisagem&#58;	 "Devoro a paisagem que me devora"; "Engoli o cosmo". Há sempre uma tentativa de enquadrar, limitar, prender, dominar a paisagem numa forma geométrica perfeita. Mas os elementos botânicos, secos, prensados e colados, insistem em fugir. A obra específica utiliza pétalas da PANC cosmos, flor muito presente na paisagem do Cerrado. Pesquisa inclui estudos botânicos para entender o comportamento estético da espécie ao longo do tempo, não são estudos interessados na utilidade da planta. A plasticidade que interessa é selvagem e não servil. 
ano: 2022
tecnica: colagem
---
<img src="/assets/obras/triz/caos.jpeg" loop=infinite alt="019 - Caos" class="img-fluid d-block">
`019 - Caos` 

<img src="/assets/obras/triz/sol.jpeg" loop=infinite alt="025 - Sol" class="img-fluid d-block">
`025 - Sol` 

<img src="/assets/obras/triz/equilibrio.jpeg" loop=infinite alt="018 - Equilíbrio" class="img-fluid d-block">
`018 - Equilíbrio` 



