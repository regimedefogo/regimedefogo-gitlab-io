---
layout: obra
title: Duas mãos - Mergulho nas origens
thumbnail: /assets/thumbnail/t-ricardo.png
artista: Ricardo Stuckert
l-bio: https://www.instagram.com/ricardostuckert/
t-bio: Ricardo Stuckert é reconhecido nacional e internacionalmente pelo seu trabalho de fotografia política e de registro de diversas etnias indígenas brasileiras. Atua como fotógrafo oficial do ex-presidente Lula desde 2003. Foi o responsável por registros importantes da política brasileira, como a posse de Dilma Rousseff e o processo de impeachment de Dilma Rousseff, materiais que foram utilizados na produção do documentário Democracia em Vertigem, dirigido por Petra Costa e indicado ao Oscar 2020 na categoria de Melhor Documentário onde Ricardo foi diretor de fotografia e de imagens exclusivas do filme. Em 2016 foi premiados no Oman 1st Internacional Photography Circuit com a medalha de ouro na categoria “Muscat – Pessoas”, subcategoria PSO (Particle Swarm Optimization) pela fotografia do índio Kaiapó no rio Xingu. Sua pesquisa fotográfica com os índios brasileiros reverberou na publicação do livro "Povos Originários - Guerreiros do Tempo" e na exposição de mesmo nome que foi exibida em Buenos Aires e Belo Horizonte. Recentemente lançou o livro "O Brasil no mundo&#58;	 8 anos de governo Lula” com registros históricos sobre as relações internacionais do Brasil nos anos de governo de Lula.
texto-descricao: <h4>Duas mãos</h4> Um entre muitos registros fotográficos feitos em visita ao Sítio Histórico e Patrimônio Cultural Kalunga (SHPCK) localizado entre os municípios de Cavalcante, Teresina de Goiás e Monte Alegre onde residem mais de duas mil famílias. Esta que é a maior comunidade remanescente de quilombo do Brasil exerce papel fundamental na preservação de uma importante área do bioma do Cerrado. Na imagem, mãos de trabalhadores Kalunga marcadas pelo tempo e por histórias de luta.<br><br><h4>Mergulho nas origens</h4>Registro fotográfico de João Pedro mergulhado nas águas da cachoeira Santa Bárbara que fica localizada dentro do território Kalunga. Seus pais nasceram no Quilonbo Kalunga do Engenho II, em Cavalcante (GO), lugar onde ele também passou boa parte da vida. 
tecnica: fotografia
---
<img src="/assets/obras/ricardo/foto2.jpeg" loop=infinite alt="Duas mãos" class="img-fluid d-block">
`Duas mãos` 
<br><br>

<img src="/assets/obras/ricardo/foto1.jpeg" loop=infinite alt="Mergulho nas origens" class="img-fluid d-block">
`Mergulho nas origens` 
<br><br>
