---
layout: obra
title: Samsara_cerrado_mutante
thumbnail: /assets/thumbnail/t-samsara.png
artista: Lynn Carone
l-bio: https://lynncarone.com/home/
t-bio: Lynn Carone é natural de Los Angeles, EUA, formou-se como artista na FAAP, SP, 1988. Atualmente vive em Brasília, leciona Artes na Universidade de Brasília (UnB) onde também é mestranda em Arte e tecnologia. Sua pesquisa artística envolve práticas de “site specificity”, fotografia, vídeo instalações, objeto e gravura. </br>Participou de exposições internacionais como “Fish eye”&frasl; Cardiff, o seminário Internacional de arte e Natureza&frasl;USP, Panoramas, Valência&frasl;Espanha e Link2021 art&design em Auckland&frasl;New Zeland e de diversas exposições coletivas e individuais no Brasil como Paço das Artes&frasl;SP, MAC de Curitiba e de Campinas, MARP de Ribeirão Preto, Sesc Amapá e Pinheiros&frasl;SP entre outros.  Dois de seus trabalhos estão nos acervos da Pinacoteca de São Paulo e da Galeria Dez de Brasília.
texto-descricao: Samsara é o trabalho que faz parte da pesquisa em andamento que aborda o conceito de sitespecificity e videoarte e arte digital. A partir de registros feitos com o celular em caminhadas em uma rua próxima ao rio Taboquinhas, revela-se a complexa relação da convivência entre humanos e não humanos, especificamente ambientados no cerrado de Brasília. A especificidade do lugar impulsionou à reflexão sobre a morte e os impactos invisíveis causados cotidianamente. A arte digital e recursos computacionais possibilitam a criação de realidades virtuais pelas quais é possível gerar pontos de fuga para recriar outras realidades e revelar tanto um desejo pessoal quanto um olhar crítico para uma situação observada. O termo Samsara em sânscrito, pode ser traduzido como “vagando”, “fluindo”, “passando”. Em muitas tradições ocidentais é a referência da passagem pela vida e o ciclo de repetição de nascimento, vida e morte, passado, presente e futuro. No Budismo também pode ser o ensinamento de ciclo de morte e renascimento da consciência de uma mesma pessoa, ou seja, da transformação pessoal. A Autopoiesi, segundo Francisco Varela e Humberto Maturana, é a capacidade dos seres de se auto produzir, assim como acontece no processo criativo do artista. Por meio de processo artísticos, é possível reinventar vidas artificialmente e relembrar a capacidade do cerrado de renascer das próprias cinzas. Mas, deve-se perguntar&#58; - quais os limites dessa resiliência?
ano: 2022
tecnica: videoarte (frames)
---
`Lagarto` 
<br><br>
<img src="/assets/obras/samsara/lagartoI.jpg" loop=infinite alt="Lagarto 1" class="img-fluid mx-auto d-block">
<br>
<img src="/assets/obras/samsara/lagartoII.jpg" loop=infinite alt="Lagarto 2" class="img-fluid mx-auto d-block">
<br>
<img src="/assets/obras/samsara/lagartoIII.jpg" loop=infinite alt="Lagarto 3" class="img-fluid mx-auto d-block">
<br>
<img src="/assets/obras/samsara/lagartoIV.jpg" loop=infinite alt="Lagarto 4" class="img-fluid mx-auto d-block">
<br>
`Sapo` 
<br><br>
<img src="/assets/obras/samsara/sapoI.jpg" loop=infinite alt="Sapo 1" class="img-fluid mx-auto d-block">
<br>
<img src="/assets/obras/samsara/sapoII.jpg" loop=infinite alt="Sapo 2" class="img-fluid mx-auto d-block">
<br>
<img src="/assets/obras/samsara/sapoIII.jpg" loop=infinite alt="Sapo 3" class="img-fluid mx-auto d-block">
<br>
<img src="/assets/obras/samsara/sapoIV.jpg" loop=infinite alt="Sapo 4" class="img-fluid mx-auto d-block">
<br>
