---
layout: obra
title: Caminhando sobre cinzas
thumbnail: /assets/thumbnail/t-caminhando.png
artista: Estúdio Canetti
l-bio: https://www.instagram.com/estudio_canetti/
t-bio: Ana Lúcia Cannetti é doutoranda no Programa de Pós Graduação em Artes visuais da Universidade de Brasília (UnB), mestre em Psicologia pela Universidade Federal de Santa Catarina (UFSC), linha de pesquisa Relações éticas, estéticas e processos de criação,  licenciada em Artes Visuais pela Universidade Estadual do Paraná/Faculdade de Artes do Paraná (UNESPAR) e psicóloga pela Universidade Federal do Paraná (UFPR). sTrabalhou no Museu Casa Alfredo Andersen e Centro Juvenil de Artes Plásticas do Paraná, ambos órgãos da Secretaria de Estado de Comunicação e da Cultura do Paraná (2008-2020). Foi docente no curso de Licenciatura em Artes Visuais da Universidade Estadual do Paraná - Campus de Curitiba II - Faculdade de Artes do Paraná (2017- 2019). 
texto-descricao:  Diante de uma terra arrasada e queimada, colher as cinzas e transformá-las. Caminhando sobre cinzas foi construída a partir das técnicas de modelagem manual e esmaltação em cerâmica de alta temperatura. Utilizou cinzas de matérias orgânicas colhidas em uma caminhada por um terreno que havia sofrido um incêndio recente, na cidade de Brasília/DF. O brilho e a cor desta peça escultórica são decorrentes da transformação destas cinzas em vidrados, após a queima cerâmica. Algumas cascas e sementes do cerrado, coletadas em caminhadas pela cidade, também foram utilizadas para realizar texturas e marcas na argila. Como se estas matérias pudessem ir caminhando pela forma da fita de Moebius, um objeto não orientável, onde não tem parte de cima ou de baixo, dentro ou fora. Fui caminhando em um fluxo contínuo, como na proposição de Lygia Clark, porém agora sobre terra, cinzas e transformações. 
ano: 2022
tecnica: escultura
---
<img src="/assets/obras/caminhando.jpeg" loop=infinite alt="Caminhando sobre cinzas" class="img-fluid d-block">
`Caminhando sobre cinzas` 
<br><br>


