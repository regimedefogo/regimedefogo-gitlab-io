---
layout: obra
title: tamboril-da-mata+ipê-amarelo
thumbnail: /assets/thumbnail/t-tamboril.png
artista: Thiago Rodeghiero
l-bio: https://www.instagram.com/thiagorodeghiero/
t-bio: Thiago Rodeghiero se descreve como um ser em deslocamento que tem a arte como um encontro consigo. Coloco-se em movimento catando coisas e fazendo delas seus artifícios de trabalho deixando-se levar pelos fluxos dos processos banais e corriqueiros. Para o artista aceitar que se está perdido é uma forma de potência geradora de acasos fecundos. Caminha bordas dos afetos, dobrando as margens e criando novas curvas para linhas até então retas, articulando matérias para criar visualidades. Constroi objetos como meio de trabalho através de vídeos e fotografias .Graduado em Artes Visuais na Universidade Federal de Pelotas (UFPel) possui mestrado em Educação pela mesma universidade . Participa de coletivos de artistas, como o Coletivo Nômade da Editora Nômade (2021).
texto-descricao: Este trabalho pensa na proliferação a partir das sementes. A frase premissa é&#58;	 “Tentaram nos enterrar, mas esqueceram que somos sementes”. As sementes são um processo de resistência e a forma mais compacta de potência das plantas. O espírito da planta está na semente. Podemos guardar uma planta por anos, basta saber como armazenar suas sementes. O seu espírito, objetivo, subjetivo e intersubjetivo, carrega a necessidade de conexão e relação, pois, sozinho, não alcança sua forma plena de potência. 
ano: 2022
tecnica: fotografia
---
<div class="menu d-none d-sm-block">


  <div id="imagens" class="carousel slide" data-ride="carousel">


   <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
     


   </ul>
    
   <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/tamboril/tamboril1.jpg" alt="Tamboril" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/tamboril/tamboril2.jpg" alt="Tamboril" class="img-fluid mx-auto d-block">
      </div>
      
  </div>
 
 
   <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
<br>





<div class="d-block d-sm-none">
  
<img src="/assets/obras/tamboril/tamboril1.jpg" alt="Tamboril" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/tamboril/tamboril2.jpg" alt="Tamboril" class="img-fluid" width="35%">
  
</div>



