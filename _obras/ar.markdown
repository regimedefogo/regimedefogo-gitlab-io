---
layout: obra
title: Ar_Ipêfeito
thumbnail: /assets/thumbnail/t-ar.png
artista: Tainá Luize
l-bio: https://www.instagram.com/tainaluize/
t-bio: Tainá Luize é formada em Design de Moda pelo Instituto Federal de Brasília (IFB) e atualmente é mestranda em Arte Eletrônica no Programa de Pós-graduação em Artes Visuais pela Universidade de Brasília (UnB). Como designer e artista atua com interesse na interseção entre Arte Computacional, Design, Arte, Moda e Interfaces. Faz parte da equipe do Medialab/UnB e já teve trabalhos coletivos expostos em locais como o Museu Nacional da República, Espaço Niemeyer e Espaço Cultural Afonso Vilaça do TCU.
texto-descricao:  A peça de computação vestível (wearable) é composta por um vestido equipado com um microcontrolador que transforma a qualidade do ar em uma visualização luminosa. Essa visualização é apresentada por meio de leds aplicados no vestido, onde os mesmos são ligados ou desligados em série de acordo com a qualidade do ar. Quanto menos gases tóxicos forem encontrados no ar, os leds são acionados e por consequência são apagados quando é detectado gases nocivos ao nosso ecossistema. O Cerrado é o segundo bioma mais importante do mundo e cada vez mais e um importante bioma para o ciclo de carbono no ecossistema mundial. A partir dessas questões mediadas pela visualização da qualidade do ar em tempo real na paisagem do cerrado,  a obra propõe  um questionamento crítico com a nossa relação com o bioma. A peça tem como inspiração para o seu design a planta Ipê-do-cerrado, um ícone do cerrado brasiliense. Seu processo de tingimento foi feito a partir das raízes do Açafrão-da-terra, que mesmo não sendo endêmica do cerrado é uma espécies bem adaptada ao bioma, nos propondo repensar como podemos ampliar a nossa percepção para além das espécies endêmicas, mas também as espécies que encontram seu refúgio no nosso cerrado. 
ano: 2019
tecnica: Objeto de vestuário interativo (wearable)
---
<div class="menu d-none d-sm-block">


  <div id="imagens" class="carousel slide" data-ride="carousel">


   <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
     


   </ul>
    
   <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/taina/ar1.jpeg" alt="Ar_ipefeito" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/taina/ar2.jpeg" alt="Ar_ipefeito" class="img-fluid mx-auto d-block">
      </div>
      
  </div>
 
 
   <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
<br>

<div class="d-block d-sm-none">
  
<img src="/assets/obras/taina/ar1.jpeg" alt="Ar_ipefeito" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/taina/ar2.jpeg" alt="Ar_ipefeito" class="img-fluid" width="35%">
  
</div>






