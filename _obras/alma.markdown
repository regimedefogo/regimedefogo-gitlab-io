---
layout: obra
title: Alma em Cinzas
thumbnail: /assets/thumbnail/t-alma.png
artista: Paula Castro
l-bio: https://www.instagram.com/paulardcastro/
t-bio: Paula Castro é artista visual integrante do coletivo de artes da Escola do Futuro Basileu França em Goiânia, Goiás.
texto-descricao: Um ente quimérico esfalece em uma árvore recém queimada. É o espírito da árvore, a essência da natureza em cinzas, também a minha e a sua. 
ano: 2022
tecnica: fotografia
---
<img src="/assets/obras/alma.jpeg" loop=infinite alt="Alma em Cinzas" class="img-fluid d-block">


