---
layout: obra
title: De quando um animal dança e grita sua dor&#58; a dança do desespero
thumbnail: /assets/thumbnail/t-animal.png
artista: Robson Castro
l-bio: https://www.robsoncastro.art.br
t-bio: Robson Castro nasceu em Betim, Minas Gerais em 1979 e hoje vive e trabalha em Brasília, DF. O artista trabalha com dança e performance á frente da Cia Inexistente, sua plataforma de criação, onde explorou até o momento temas relacionados à origem, corpo e a relação com sua própria história. Junto à Anti Status Quo Companhia de Dança (ASQ), participou de pesquisas relacionadas a corpo e cidade. Mestrando em Artes pela Universidade de Brasília (unB). Junto da ASQ e do Teatro do Concreto, participou de vários festivais de dança e de teatro no Brasil e em países da Europa (Suíça, Eslovênia e Sérvia). 
texto-descricao: De quando um animal dança e grita sua dor&#58; a dança do desespero é a dança da dor que vem da judiação com a mãe terra. De toda a tomada de terra dos povos originários e da comercialização predatória da terra feita por todo o tempo, desde que invadiram (invadimos) esse país. Do descuido, das queimadas, do mal uso, da má distribuição. Da fome e da pobreza dentro do país rico que somos. Essa contradição que não cabe no corpo, na voz, na alma. Esse desespero de não saber exatamente o que fazer ou, pior ainda, de tentar fazer algo mas que é muito pouco diante da força do capital. É um grito, uma dor, uma dança do desespero, um desalento. Assim, o trabalho se aproxima do tema da exposição "Regime de Fogo" pois traz no corpo, na voz e na imagem, um pouco dessa dor que o fogo predatório traz para os animais mais sensíveis, sejam eles bípedes ou não. Uma dor sem tamanho, um mal sem remédio, e uma desesperança que assola meu corpo, minha voz, minha alma.  
ano: 2022
tecnica: Vídeo-performance
---
<iframe width="832" height="468" src="https://www.youtube.com/embed/xjpFnzQPkuw" title="Seca em pé, água revolta - Clarice Martins" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

