---
layout: obra
title: Pertencimento
thumbnail: /assets/thumbnail/t-pertencimento.png
artista: Clarice Gonçalves
l-bio: https://www.claricegoncalves.com/
t-bio: Clarice Gonçalves é artista visual e mãe solo, vive e trabalha em Taguatinga- DF, é graduada em Artes Visuais pela Universidade de Brasília (UnB) e desde de 2005 apresenta sua pesquisa pictórica em mostras individuais e coletivas, feiras e premiações pelo país e afora. Sua produção aborda temas como socialização, sexualidade, maternidade e animalidade em respectivas performances dentro do contexto social como reflexo de sua pesquisa poética em torno de suas vivências corporais. Seu meio principal de materialização é pictórico, embora cada vez mais os processos performáticos tenham feito parte da criação dessas imagens em pintura através da dança, vídeo e fotografia. Clarice é integrante do Coletivo Matriz, um coletivo de mães artistas do DF que desenvolve lambes e intervenções urbanas na cidade. Artista finalista do prêmio PIPA online 2022.
texto-descricao: Essa obra foi criada durante a pandemia, numa busca de acolhimento, respiro, integração, tendo como referência uma fotografia tirada no rio São Miguel na Chapada dos Veadeiros. Como mãe solo soterrada pelo trabalho de cuidado e sufocamento pandêmicos, precisei transportar meu corpo por vezes física e tb pictoricamente para lugares onde receberia o que a sociedade não estava me proporcionando, acolhimento, pertencimento, colo. Esse movimento gerou o compilado de obras ´Idìlio´ com várias imagens de corpos em integração com o meio natural, com o cerrado.
ano: 2020
tecnica: pintura
---
<img src="/assets/obras/pertencimento.jpeg" loop=infinite alt="Pertencimento" class="img-fluid mx-auto d-block">
<br>

`Dimensão original :  100x63cm` 



