---
layout: obra
title: “Encontrar os tesouros e reparar as ausências”
thumbnail: /assets/thumbnail/t-encontrar.png
artista: Marta Mencarini
l-bio: https://www.instagram.com/marta.mencarini.art/
t-bio: Marta Mencarini Guimarães nasceu, reside e trabalha em Brasília - DF. Doutoranda em Arte pela Universidade de Brasília (UnB) na linha Poéticas Transversais com bolsa CAPES. Artista visual, professora, pesquisadora e mãe, desenvolve pesquisas sobre feminismos e maternagem na arte contemporânea, abordando a maternidade enquanto experiência potencial e política. Investiga a poética artística em auto narrativas e auto ficções. Tem experiência na área de artes, transitando pela história da arte, pintura, fotografia, performance, intervenção urbana, arte tecnologia e vídeo. Integrante do Coletivo Matriz e do Grupo Mesa de Luz. Co - coordena o projeto Mapeamento Arte e Maternagem (AeM). 
texto-descricao: “Encontrar os tesouros e reparar as ausências” (2021). Marta Mencarini, óleo sobre tela 54x65cm. A pintura trafega pela construção de experiências-memórias a partir da relação intrafamiliar e ancestral com a terra. O quintal de plantas nativas do cerrado, vegetais e árvores frutíferas, envolvem a paisagem lúdica das brincadeiras infantis em construir mundos imaginários e aventuras mirabolantes de encontrar tesouros escondido. Gerações distintas, avô-neta, envolvem-se em ensinar-conhecer-experimentar em devir vivência direta retroalimentação afetiva, da infância presente-passado-futuro, dos desejos/sonhos infantis possíveis e impossíveis. Dos caminhos-escolhas traçados em outros tempos, possíveis de serem (re)feitos no agora.  
ano: 2021
tecnica: pintura
---

<img src="/assets/obras/encontrar.jpeg" loop=infinite alt="Encontrar os tesouros e reparar as ausências" class="img-fluid mx-auto d-block">
<br>

`Dimensão original :  54x65cm` 

