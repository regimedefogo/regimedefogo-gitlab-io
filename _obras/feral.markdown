---
layout: obra
title: FERAL
thumbnail: /assets/thumbnail/t-feral.png
artista: Ianni Luna
l-bio: https://ianniluna.net/
t-bio: Ianni Luna é artista e pesquisadora do som. Atualmente desenvolve uma pesquisa de pós-doutorado sobre arte sonora na Universidade de Brasília (UnB). Doutora em Artes (2020), mestre em História (2006), bacharel em Artes Plásticas (2013), bacharel em Antropologia (2003); todos pela Universidade de Brasília (UnB). É professora de Artes da Secretaria de Educação do Distrito Federal (SEEDF). Sua atual pesquisa envolve justaposições entre som e imagem que ativem sentidos e potências para poéticas relacionais. Desenvolve trabalhos em instalação, vídeo arte e performance sonora. Desde 2011 tem participado de exposições coletivas e eventos de performance no Brasil e mais recentemente, em Portugal, Viena e Berlim. 
texto-descricao: Este trabalho articula arte, tecnologia e ficção como eixos significadores de uma prática poética da linguagem do cinema experimental. Aqui o ambiente, a natureza e o cerrado compõem uma estória não linear, que atravessa paisagens sobrepostas, assumindo um caráter de fábula, uma epopéia trans-humana que opera por meio de deslocamentos no espaço-tempo. Como um road movie, estabelece uma trajetória pelos meandros da memória selvagem enquanto construção imaginária de um encontro com o que ainda não foi vivido. As imagens reverberam uma trilha sonora que ecoa uma espécie de viagem aberta, que entrecruza devaneios por lugares íntimos inéditos.   
ano: 2021
tecnica: videoarte
---

<iframe width="832" height="468" src="https://www.youtube.com/embed/umyBceKU5EU" title="Seca em pé, água revolta - Clarice Martins" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
