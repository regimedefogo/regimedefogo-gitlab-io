---
layout: obra
title: República dos pequis
thumbnail: /assets/thumbnail/t-republica.png
artista: Priscila Rezende Portugal
l-bio: http://portugalpriscila.weebly.com/
t-bio: Priscila Rezende Portugal é artista, mestranda em Gestão e Organização do Conhecimento pela Universidade Federal de Minas Gerais (UFMG), graduada em Artes Plásticas pela Escola Guignard da Universidade Estadual de Minas Gerais(UEMG). Tem habilitação em Serigrafia (2019) e Fotografia (2020). Trabalha com gravura expandida, fotografia e novas mídias. É membro do grupo de pesquisa, desenvolvimento e inovação Laboratório de Poéticas Fronteiriças [labfront.tk](CNPq/UEMG), no qual pesquisa os atravessamentos entre arte, ciência e tecnologia.
texto-descricao:  A obra desenvolvida em formato de cartaz digital brinca com expressões coloquiais do vocabulário brasileiro como "república das bananas" e "yes, nos temos bananas", trocando pela fruta pequi. O pequi (caryocar brasiliense) é fruto do cerrado brasileiro e é encontrado com abundância no bioma. Assim como a banana foi escolhida para representar a abundância no clima tropical brasileiro, o pequi ainda carrega a brasilidade em seu nome científico. Em suas características físicas, pode ser difícil de engolir, assim como o Brasil.Os cartazes são elementos presentes nas ruas das cidades brasileiras e ainda são muito utilizados por horti frutis, conhecidos como sacolões.  
ano: 2022
tecnica: digital
---
<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">


   <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>

   </ul>
    
   <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/priscila/pequi1.png" alt="República dos pequis" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/priscila/pequi2.png" alt="República dos pequis" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/priscila/pequi3.png" alt="República dos pequis" class="img-fluid mx-auto d-block">
      </div>

    </div>
    
   <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
<br>






<div class="d-block d-sm-none">
  
<img src="/assets/obras/priscila/pequi1.png" alt="República dos pequis" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/priscila/pequi2.png" alt="República dos pequis" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/priscila/pequi3.png" alt="República dos pequis" class="img-fluid" width="35%"><br><br>
</div>



