---
layout: obra
title: Espírito de árvore encarnada 
thumbnail: /assets/thumbnail/t-espirito.png
artista: Jurema
l-bio: https://www.instagram.com/juremagrita/
t-bio: Baiana em Goiânia, sobrevivo e resisto há 08 anos no Estado de Goiás. Formada em Serviço Social e mais algumas formações inacabadas, das quais colhi muito conhecimento, e as quais me trouxeram até aqui, hoje aos 33 anos de idade, afastada da minha profissão de mazelas para cuidar da mente, entendi que por ser natureza não é possível ficar bem nesse mundo e no contexto socio-político que estamos inseridos. A natureza está sendo atacada covardemente e nós, que somos parte dela, também estamos morrendo. É preciso reagir.
texto-descricao:  Espírito de Árvore solitária que ronda pelos cantos do cerrado a procura de vida e apoio para o embate contra seus destruidores. Jurema ouve cantos mudos de dor, rastros apagados pelo fogo, raízes arrancadas sem piedade, ar rarefeito que invade, a seiva o sangue que corre - corroendo suas veias sem nutrientes de uma terra que já sente a mão pesada do Homem/Fogo/Horror que arde.
ano: 2022
tecnica: videoarte
---
<iframe width="832" height="468" src="https://www.youtube.com/embed/lN7991qU4T8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

