---
layout: obra
title: escutar os ossos
thumbnail: /assets/thumbnail/t-escutar.png
artista: Tatiana Duarte
l-bio: https://sites.google.com/view/tatianaduarte/desenterrando-afetos
t-bio:  Tatiana Duarte é performer busca forças nos feminismos nômades para desenterrar algo que não foi dito&#58; de um apagamento. Uma aprendiz redescobrindo caminhos e fissuras. Doutoranda em Artes visuais pela Universidade de Brasília (UnB), mestra em Artes Visuais e licenciada em Teatro pela Universidade Federal de Pelotas (UFPel). Pesquisa os processos de criação e poéticas do cotidiano, coloca o corpo como suporte, redescobrindo caminhos e aberturas, desenterrando algo que não foi falado, através da voz que retira as camadas dos apagamentos. Uma aprendiz que reencontra caminhos e sedimentação percorrendo territórios entre mapas e vias que são pinçados ao longo do fazer nos processos de criação, trazendo à tona as relações do cotidiano e colocando o corpo como suporte. 
texto-descricao: Trago as folhas secas como uma escuta dos ossos, expondo a retirada de camadas do que não sei dizer com as palavras. Um gesto que é expressão das sensações oriundas das ações entre o cerrado, o fogo, a terra, e os elementos. O fogo é um aviso, deixando os ossos secos. A prudência é o tempo, que precisa escutar a si. 
ano: 2022
tecnica: videoperformance 
---
<iframe width="832" height="468" src="https://www.youtube.com/embed/sCN1VLFIbDk" title="Seca em pé, água revolta - Clarice Martins" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>