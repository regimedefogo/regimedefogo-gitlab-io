---
layout: obra
title: trans_substânciação
thumbnail: /assets/thumbnail/t-trans.png
artista: José Loures
l-bio: https://www.joseloures.com
t-bio: José Loures é artista multimídia, professor de Histórias em Quadrinhos, e produtor cultural. Doutor em Artes pela Universidade de Brasília (UnB). Mestre em Arte e Cultura Visual pela Universidade Federal de Goiás (UFG). Especialista em Docência na Educação Profissional, Técnica e Tecnológica pelo Instituto Federal de Goiás (IFG). Graduado em Artes Visuais pela Universidade Federal de Goiás (UFG). Licenciado em Formação Pedagógica Para Graduados Não Licenciados pelo Instituto Federal de Goiás (IFG).  Membro do CIPEG - Coletivo Interdisciplinar de Pesquisa em Games. Professor de Artes no Instituto Federal de Educação, Ciência e Tecnologia de São Paulo (IFSP). Trabalha na linguagem da arte computacional, histórias em quadrinhos, webarte, fake arte e gamearte. Também pesquisa sobre transhumanismo, videogames, jogos de tabuleiro,educação, cibercultura, sexualidade e práticas divinatórias.
texto-descricao: A imaginação e criatividade humanas são capazes de transformar pensamentos e reflexões em Arte. Nesse sentido, quais as reflexões artísticas de uma inteligência artificial sobre frases ditas por políticos brasileiros sobre a preservação da Natureza? Manchetes em noticiários envolvendo a devastação do Cerrado? Trans_substanciação envolveu a orientação de uma inteligência artificial para criação de cinco pinturas sobre o contexto político e ambiental no Brasil. Através de algoritmos compreendemos a percepção de uma inteligência artificial sobre temas tão sensíveis e urgentes. 
ano: 2022
tecnica: arte computacional
---
<img src="/assets/obras/trans/because_they_only_talk_about_covid_and_pass_the_her.png" loop=infinite alt="Because they only talk about covid and pass the her" class="img-fluid d-block">
`Because they only talk about covid and pass the her` 
<br><br>

<img src="/assets/obras/trans/lost_31_thousand_soccer_fields_in_the_Cerrado.png" loop=infinite alt="Lost 31 thousand soccer fields in the Cerrado" class="img-fluid d-block">
`Lost 31 thousand soccer fields in the Cerrado` 
<br><br>

<img src="/assets/obras/trans/I_am_passionate_about_the_Cerrado_and_I_was_very_sa.png
" loop=infinite alt="I am passionate about the Cerrado and I was very sa" class="img-fluid d-block">
`I am passionate about the Cerrado and I was very sa` 
<br><br>

<img src="/assets/obras/trans/ox_is_the_firefighter_of_the_Pantanal.png" loop=infinite alt="Ox is the firefighter of the Pantanal" class="img-fluid d-block">
`Ox is the firefighter of the Pantanal` 
<br><br>

<img src="/assets/obras/trans/the_fruit_is_widely_used_in_country_cuisine.png" loop=infinite alt="The fruit is widely used in country cuisine" class="img-fluid d-block">
`The fruit is widely used in country cuisine` 
<br><br>
