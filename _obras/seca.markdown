---
layout: obra
title: Seca em pé, água revolta
thumbnail: /assets/thumbnail/t-seca.png
artista: Clarice Martins
l-bio: https://www.instagram.com/claricelm/
t-bio: Clarice Martins faz parte do grupo de audiovisual Capivara do Ar, tendo colaborado com roteiro, direção, edição e assistência de fotografia e som nas produções do grupo. É estudante de teoria, crítica e história da arte na Universidade de Brasília (UnB). Em sua formação acadêmica, se interessa por trabalhos de temática humano x natureza, principalmente pelas reflexões artísticas sobre a relação entre humanos e animais não humanos.
texto-descricao: “Que cidade é essa em que vim parar? Que espécies de setorizações são essas? Uma viajante investiga impressões sobre uma cidade dividida por seu tempo, clima e por suas pessoas.” O curta explora os meios naturais e urbanos do cerrado, suas histórias estrangeiras e nativas, seus paralelos políticos e poéticos, onde suas divisas se encontram.
ano: 2020
tecnica: videoarte
---
<iframe width="832" height="468" src="https://www.youtube.com/embed/iI_WbPuTU0g" title="Seca em pé, água revolta - Clarice Martins" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>