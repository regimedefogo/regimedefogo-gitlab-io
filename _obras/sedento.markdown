---
layout: obra
title: Sedento
thumbnail: /assets/thumbnail/t-sedento.png
artista: André Inácio
l-bio: https://www.instagram.com/inaciomoura13/
t-bio: Fazedor cultural.
texto-descricao:  A obra se passa na região do cerrado brasileiro, na superfície do extinto aquífero Guarani e Urucuia.  O personagem principal é o espirito  do falecido Rio das éguas que paira pelo seu leito sedento por justiça. 
ano: 2022
tecnica: fotografia
---
<img src="/assets/obras/sedento.png" loop=infinite alt="Sedento" class="img-fluid d-block">



