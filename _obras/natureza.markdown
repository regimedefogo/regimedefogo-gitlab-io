---
layout: obra
title: Natureza em Nós
thumbnail: /assets/thumbnail/t-natureza.png
artista: Viníciu Fagundes
l-bio: https://www.instagram.com/estudio_tutoia/
t-bio: Viníciu Fagundes escolheu uma profissão que possibilitasse atuação direta com o meio ambiente graduando-se em Engenharia Ambiental pela Pontífica Universidade Católica de Goiás (PUC- GO) e posteriormente se pós-graduando como Mestre em Engenharia do Meio Ambiente e Doutor em Ciências Ambientais pela Universidade Federal de Goiás (UFG). Há cerca de três anos, com o advento da Pandemia da COVID-19, sentiu a necessidade de se expressar artisticamente de forma autodidata e independente, quando passou a imprimir sua visão de mundo em composições criadas com elementos naturais associados à cultura popular, na tentativa de provocar reflexões sobre a crise ambiental atual e a necessidade de resgatarmos aspectos culturais esquecidos nos corredores do tempo. Para tanto, utiliza referências de elementos orgânicos oriundos de áreas naturais e urbanas (praças, parques, canteiros de avenidas e até mesmo quintais) de diferentes biomas, notadamente do Cerrado e da Amazônia, enxergados por ele como universos dinâmicos, belos, plenos e absolutos aos quais a sociedade necessita se reconectar. Suas obras são embasadas em pesquisa artística incansável e em evolução, e são feitas mediante o emprego de técnicas mistas, predominando o assemblage associado à pintura de elementos naturais, isolados ou não, sobrepostos ou não. Mantém estúdio (@estudio_tutoia) em Goiânia – Goiás, onde reside. Embora a arte seja um caminho relativamente recente em sua vida, o envolvimento com este universo tem sido intenso.
texto-descricao: <h4>Natureza em Nós&#58; Um Ninho Chamado Brasil</h4> Denominada ‘Natureza em Nós&#58; Um Ninho Chamado Brasil’, a obra é constituída de alguns dos elementos vegetais mais bonitos oriundos do Cerrado e da Amazônia (verdadeiras gemas vegetais produzidas por nossas florestas), empregados por muitos brasileiros em diferentes aspectos de suas vidas, como a Jarina, o Buriti, o Açaí, o Cerú, o Jerivá, o Jupati, o Olho-de-Boi, o Olho-de-Cabra... Ninhos são habilidosamente construídos por diversas espécies de animais, principalmente aves. São feitos parte a parte, elemento a elemento, resultando em estruturas complexas que podem ser consideradas verdadeiras obras arquitetônicas da natureza. Representam casa, lar, cuidado, aconchego, acolhimento, dedicação... Também simbolizam esperança no novo, no amanhã, no que está por vir, em novas ideias, novos comportamentos, novas concepções de mundo e de vida... ‘Natureza em Nós&#58; Um Ninho Chamado Brasil’ é uma obra que representa tudo isso e muito mais, a depender do olhar de cada um. É um convite à reflexão sobre a dicotomia crise ambiental x desenvolvimento econômico; sobre a cultura do “descarte”; sobre quais “riquezas” precisamos efetivamente valorizar nos dias de hoje; sobre o uso que nossos antepassados faziam da natureza; sobre o que, de fato, nos complementa, nos une, nos reconecta a nós mesmos e ao todo... Por outro lado, nos estimula a refletirmos sobre o quanto de natureza reconhecemos e cultivamos internamente, e sobre o que estamos fazendo com nossos ecossistemas. Quanto conhecemos da natureza que nos cerca? Quanto trazemos/carregamos de natureza em nós? Me reconheço natureza? Somos povo-natureza? Ou natureza-povo? Ou nada disso? Natureza&#58; eu, tu, nós? Meu olhar se acostumou aos detalhes do mundo natural aparentemente insignificantes para a maioria das pessoas, mas grandiosos, tocantes e envolventes para mim. A natureza que existe em mim saúda a natureza que existe em ti. Sejam bem-vindos ao meu mundo, à minha floresta interna... <br><br><h4>Natureza em Nós&#58; Jequitibás</h4> Denominada ‘Natureza em Nós&#58; Jequitibás’, a obra é majoritariamente constituída de cápsulas de Jequitibás, a maior espécie de árvore do Brasil, um verdadeiro gigante das florestas que abriga centenas de organismos e que é capaz de viver por séculos. Suas cápsulas eram muito utilizadas pelos “antigos” na confecção de cachimbos (como o do próprio Saci-pererê, na lenda), e como “binga”, um tipo de isqueiro rudimentar. Especificamente, ‘Natureza em Nós&#58; Jequitibás’ é um convite à reflexão sobre a dicotomia crise ambiental x desenvolvimento econômico; sobre a cultura do “descarte”; sobre quais “riquezas” precisamos efetivamente valorizar nos dias de hoje; sobre o uso que nossos antepassados faziam da natureza; sobre o que, de fato, nos complementa, nos une, nos reconecta a nós mesmos e ao todo... Por outro lado, nos estimula a refletirmos sobre o quanto de natureza reconhecemos e cultivamos internamente, e sobre o que estamos fazendo com nossos ecossistemas. Quanto conhecemos da natureza que nos cerca? Quanto trazemos/carregamos de natureza em nós? Me reconheço natureza? Somos povo-natureza? Ou natureza-povo? Ou nada disso? Natureza&#58; eu, tu, nós? Meu olhar se acostumou aos detalhes do mundo natural aparentemente insignificantes para a maioria das pessoas, mas grandiosos, tocantes e envolventes para mim. A natureza que existe em mim saúda a natureza que existe em ti. Sejam bem-vindos ao meu mundo, à minha floresta interna...
ano: 2021
tecnica: composição assemblage
---
<div class="menu d-none d-sm-block">
<h3>Natureza em Nós: Um Ninho Chamado Brasil</h3>

  <div id="imagens" class="carousel slide" data-ride="carousel">


   <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
      <li data-target="#imagens" data-slide-to="4"></li>


   </ul>
    
   <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/viniciu/ninho1.jpeg" alt="Natureza em Nós: Um Ninho Chamado Brasil" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/viniciu/ninho2.jpeg" alt="Natureza em Nós: Um Ninho Chamado Brasil" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/viniciu/ninho3.jpeg" alt="Natureza em Nós: Um Ninho Chamado Brasil" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/viniciu/ninho4.jpeg" alt="Natureza em Nós: Um Ninho Chamado Brasil" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/viniciu/ninho5.jpeg" alt="Natureza em Nós: Um Ninho Chamado Brasil" class="img-fluid mx-auto d-block">
      </div>
    </div>
    
   <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>


<div id="imagens2" class="carousel slide" data-ride="carousel">

   <ul class="carousel-indicators">
      <li data-target="#imagens2" data-slide-to="0" class="active"></li>
      <li data-target="#imagens2" data-slide-to="1"></li>
      <li data-target="#imagens2" data-slide-to="2"></li>


   </ul>

   <div class="carousel-inner">
   	   <h3>Natureza em Nós: Jequitibás</h3>
      <div class="carousel-item active">
        <img src="/assets/obras/viniciu/jequitiba1.jpeg" alt="Natureza em Nós: Jequitibás" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/viniciu/jequitiba2.jpeg" alt="Natureza em Nós: Jequitibás" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/viniciu/jequitiba3.jpeg" alt="Natureza em Nós: Jequitibás" class="img-fluid mx-auto d-block">>
      </div>
  
    </div>
    
   <a class="carousel-control-prev" href="#imagens2" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens2" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>


</div>







<div class="d-block d-sm-none">
  <h3>Natureza em Nós: Um Ninho Chamado Brasil</h3>
  
<img src="/assets/obras/viniciu/ninho1.jpeg" alt="Natureza em Nós: Um Ninho Chamado Brasil" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/viniciu/ninho2.jpeg" alt="Natureza em Nós: Um Ninho Chamado Brasil" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/viniciu/ninho3.jpeg" alt="Natureza em Nós: Um Ninho Chamado Brasil" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/viniciu/ninho4.jpeg" alt="Natureza em Nós: Um Ninho Chamado Brasil" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/viniciu/ninho5.jpeg" alt="Natureza em Nós: Um Ninho Chamado Brasil" class="img-fluid" width="35%">
  <h3>Natureza em Nós: Jequitibás</h3>
  <img src="/assets/obras/viniciu/jequitiba1.jpeg" alt="Natureza em Nós: Um Ninho Chamado Brasil" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/viniciu/jequitiba2.jpeg" alt="Natureza em Nós: Um Ninho Chamado Brasil" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/viniciu/jequitiba3.jpeg" alt="Natureza em Nós: Um Ninho Chamado Brasil" class="img-fluid" width="35%">
</div>


