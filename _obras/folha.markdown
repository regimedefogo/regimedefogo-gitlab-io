---
layout: obra
title: folha pequi
thumbnail: /assets/thumbnail/t-folha.png
artista: Ana Hoeper
l-bio: http://www.vimeo.com/anahoeper
t-bio:  Ana é formada em Audiovisual pela Universidade de Brasília (UnB). Trabalha com montagem de documentários e animação experimental.
texto-descricao: Vídeo 45 segundos. fotos de 105 folhas de pequi secas recolhidas pela rua. cada folha de pequi tem uma história a contar.
ano: 2022 
tecnica: videoarte
---
<iframe width="832" height="468" src="https://www.youtube.com/embed/jzFGkN8t4CA" title="Seca em pé, água revolta - Clarice Martins" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

