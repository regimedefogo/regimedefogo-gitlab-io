---
layout: obra
title: Memória
thumbnail: /assets/thumbnail/t-memoria.png
artista: Nycacia
l-bio: https://www.instagram.com/nycacia/
t-bio: Nycacia é estudante de licenciatura em Artes Visuais na Universidade de Brasília (UnB). Tem interesse em fotografia cotidiana e seus registros resultam de um olhar receptivo ao lugar onde se encontra com interposição de memórias de onde veio.
texto-descricao:  Trata-se de registros onde o principal elemento é a natureza, que é visível na maior parte das fotografias, deixando outros objetos e figuras fora de destaque e até imersos no cenário capturado. As fotografias têm origem digital e passaram por um processo de revelação com pigmentos vegetais chamado antotipia. O pigmento utilizado foi o açafrão, oriundo da índia, que se adaptou ao clima do cerrado e hoje é cultivado nesse bioma. O processo da antotipia resulta em impressões monocromáticas e efêmeras, pois com o passar do tempo se apagam do papel. As imagens registradas e aqui apresentadas, revelam um aspecto de recordação, como indícios de algo que aconteceu no cotidiano de alguém, suas memórias.
ano: 2022
tecnica: fotografia
---
<img src="/assets/obras/memoria.png" loop=infinite alt="Mémoria" class="img-fluid d-block">



